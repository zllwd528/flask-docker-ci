"""
    author: lezhang
    create: 2021-07-29 16:15
    file: main
"""
from flask import Flask

app = Flask(__name__)


@app.route('/test')
def test():
    return 'Hello world!'


if __name__ == "__main__":
    app.run('0.0.0.0', port=8080)